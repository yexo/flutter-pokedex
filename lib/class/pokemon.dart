import 'package:flutter/material.dart';
import 'package:flutter_pokedex/class/evolution.dart';

class Pokemon {
  late int id;
  late String num;
  late String name;
  late String img;
  late List<String> type;
  late String height;
  late String weight;
  late List<String>? weaknesses;
  late List<Evolution>? prevEvolution;
  late List<Evolution>? nextEvolution;
  late Color backgroundColor;
  late Color backgroundColor2;
  late bool favorite;

  Pokemon.convertJson(Map<String, dynamic> json) {
    id = json['id'];
    num = json['num'];
    name = json['name'];
    img = json['img'];
    type = json['type'].cast<String>();
    height = json['height'];
    weight = json['weight'];
    weaknesses = json['weaknesses'].cast<String>();
    prevEvolution = [];
    json['prev_evolution']
        ?.forEach((x) => {prevEvolution!.add(Evolution.convertJson(x))});
    nextEvolution = [];
    json['next_evolution']
        ?.forEach((x) => {nextEvolution!.add(Evolution.convertJson(x))});
    backgroundColor = getColor(json['type'][0] as String);
    if (json['type'].length >= 2) {
      backgroundColor2 = getColor(json['type'][1] as String);
    } else {
      backgroundColor2 = backgroundColor;
    }
    favorite = false;
  }

  static getColor(String currentType) {
    Color color;
    switch (currentType) {
      case "Grass":
        color = const Color.fromRGBO(53, 192, 74, 1);
        break;
      case "Fire":
        color = const Color.fromRGBO(255, 152, 63, 1);
        break;
      case "Ice":
        color = const Color.fromRGBO(75, 210, 193, 1);
        break;
      case "Flying":
        color = const Color.fromRGBO(138, 172, 228, 1);
        break;
      case "Psychic":
        color = const Color.fromRGBO(248, 88, 136, 1);
        break;
      case "Electric":
        color = const Color.fromRGBO(248, 208, 48, 1);
        break;
      case "Rock":
        color = const Color.fromRGBO(201, 183, 135, 1);
        break;
      case "Bug":
        color = const Color.fromRGBO(1168, 184, 32, 1);
        break;
      case "Poison":
        color = const Color.fromRGBO(182, 103, 207, 1);
        break;
      case "Normal":
        color = const Color.fromRGBO(146, 155, 163, 1);
        break;
      case "Fighting":
        color = const Color.fromRGBO(225, 44, 106, 1);
        break;
      case "Ground":
        color = const Color.fromRGBO(233, 115, 51, 1);
        break;
      case "Water":
        color = const Color.fromRGBO(51, 147, 221, 1);
        break;
      case "Dragon":
        color = const Color.fromRGBO(0, 112, 202, 1);
        break;
      case "Dark":
        color = const Color.fromRGBO(91, 83, 102, 1);
        break;
      case "Steel":
        color = const Color.fromRGBO(90, 143, 163, 1);
        break;
      case "Fairy":
        color = const Color.fromRGBO(251, 138, 236, 1);
        break;
      default:
        color = Colors.red;
    }
    return color;
  }
}

// Exemple structure JSON
// {
//     "id": 2,
//     "num": "002",
//     "name": "Ivysaur",
//     "img": "http://www.serebii.net/pokemongo/pokemon/002.png",
//     "type": [
//       "Grass",
//       "Poison"
//     ],
//     "height": "0.99 m",
//     "weight": "13.0 kg",
//     "candy": "Bulbasaur Candy",
//     "candy_count": 100, // Nullable
//     "egg": "Not in Eggs",
//     "spawn_chance": 0.042,
//     "avg_spawns": 4.2,
//     "spawn_time": "07:00",
//     "multipliers": [
//       1.2,
//       1.6
//     ],
//     "weaknesses": [
//       "Fire",
//       "Ice",
//       "Flying",
//       "Psychic"
//     ],
//     "prev_evolution": [{ 
//       "num": "001",
//       "name": "Bulbasaur"
//     }],
//     "next_evolution": [{
//       "num": "003",
//       "name": "Venusaur"
//     }]
//   }