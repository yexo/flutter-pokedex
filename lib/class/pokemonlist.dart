import 'package:flutter_pokedex/class/pokemon.dart';

class PokemonList {
  List<Pokemon> list = [];

  PokemonList.convertJson(Map<String, dynamic> json) {
    json['pokemon'].forEach((v) {
      list.add(Pokemon.convertJson(v));
    });
  }
}
