class Evolution {
  late String? num;
  late String? name;

  Evolution({this.num, this.name});

  Evolution.convertJson(Map<String, dynamic> json) {
    num = json['num'];
    name = json['name'];
  }
}
