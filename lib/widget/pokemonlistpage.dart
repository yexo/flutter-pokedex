import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_pokedex/class/pokemon.dart';
import 'package:flutter_pokedex/class/pokemonlist.dart';
import 'package:flutter_pokedex/widget/pokemondetailpage.dart';
import 'package:cached_network_image/cached_network_image.dart';

class PokemonListPage extends StatefulWidget {
  const PokemonListPage({Key? key}) : super(key: key);

  @override
  State<PokemonListPage> createState() => _PokemonListPageState();
}

class _PokemonListPageState extends State<PokemonListPage> {
  var url = Uri.parse(
      'https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json');

  Icon customIcon = const Icon(Icons.search);
  Widget customSearchBar = const Image(
    height: 40,
    image: NetworkImage(
      'https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/1200px-International_Pok%C3%A9mon_logo.svg.png',
    ),
  );
  PokemonList? pokemonList;
  List<Pokemon>? pokemonFound;
  bool showFavorite = false;

  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() async {
    var httpResponse = await http.get(url);
    var jsonDecoded = jsonDecode(httpResponse.body);
    //print(httpResponse.body);
    pokemonList = PokemonList.convertJson(jsonDecoded);
    pokemonFound = pokemonList!.list;
    //print(pokemonList!.pokemonList);
    setState(() {});
  }

  void init() {
    List<Pokemon> results;
    results = pokemonList!.list;

    setState(() {
      pokemonFound = results;
    });
  }

  void searchByName(String name) {
    List<Pokemon> results;
    if (name.isEmpty) {
      results = pokemonList!.list;
    } else {
      results = pokemonList!.list
          .where((pokemon) =>
              pokemon.name.toLowerCase().startsWith(name.toLowerCase()))
          .toList();
    }
    setState(() {
      pokemonFound = results;
    });
  }

  void showOnlyFavorite() {
    bool showFavoriteVar;
    List<Pokemon> favorites;
    if (showFavorite) {
      favorites = pokemonList!.list;
      showFavoriteVar = false;
    } else {
      favorites = pokemonList!.list
          .where((pokemon) => pokemon.favorite == true)
          .toList();
      showFavoriteVar = true;
    }

    setState(() {
      pokemonFound = favorites;
      showFavorite = showFavoriteVar;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: customSearchBar,
          actions: [
            IconButton(
              onPressed: () {
                setState(() {
                  if (customIcon.icon == Icons.search) {
                    customIcon = const Icon(
                      Icons.cancel,
                    );
                    customSearchBar = ListTile(
                      leading: const Icon(
                        Icons.search,
                        color: Colors.white,
                        size: 28,
                      ),
                      title: TextField(
                        onChanged: (name) => searchByName(name),
                        decoration: const InputDecoration(
                          hintText: 'Search by name ...',
                          hintStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontStyle: FontStyle.italic,
                          ),
                          border: InputBorder.none,
                        ),
                        style: const TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    );
                  } else {
                    customIcon = const Icon(Icons.search);
                    customSearchBar = const Image(
                      height: 40,
                      image: NetworkImage(
                        'https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/1200px-International_Pok%C3%A9mon_logo.svg.png',
                      ),
                    );
                  }
                });
              },
              icon: customIcon,
            )
          ],
          centerTitle: true,
        ),
        body: pokemonFound == null
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : GridView.count(
                crossAxisCount: 3,
                children: pokemonFound!
                    .map((pokemon) => Padding(
                          padding: const EdgeInsets.all(4),
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => PokemonDetailPage(
                                            pokemon: pokemon,
                                          )));
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  gradient: LinearGradient(
                                    begin: Alignment.topRight,
                                    end: Alignment.bottomLeft,
                                    colors: [
                                      pokemon.backgroundColor,
                                      pokemon.backgroundColor2,
                                    ],
                                  )),
                              child: Column(
                                children: <Widget>[
                                  const SizedBox(
                                    height: 15,
                                  ),
                                  CachedNetworkImage(
                                    imageUrl: pokemon.img,
                                    imageBuilder: (context, imageProvider) =>
                                        Container(
                                      height: 70,
                                      width: 70,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          image: imageProvider,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    placeholder: (context, url) =>
                                        const CircularProgressIndicator(),
                                    errorWidget: (context, url, error) =>
                                        const Icon(Icons.error),
                                  ),
                                  const SizedBox(
                                    height: 6,
                                  ),
                                  Text(
                                    pokemon.name,
                                    style: const TextStyle(
                                        fontSize: 12,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ))
                    .toList(),
              ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              const DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.red,
                ),
                child: Image(
                  image: NetworkImage(
                      'https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/1200px-International_Pok%C3%A9mon_logo.svg.png'),
                ),
              ),
              ListTile(
                title: const Text('Liste des Pokémons'),
                onTap: () {},
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
            elevation: 0.0,
            child: const Icon(Icons.favorite),
            onPressed: () {
              showOnlyFavorite();
            }));
  }
}
