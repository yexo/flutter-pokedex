import 'package:flutter/material.dart';
import 'package:flutter_pokedex/class/pokemon.dart';
import 'package:cached_network_image/cached_network_image.dart';

// ignore: must_be_immutable
class PokemonDetailPage extends StatefulWidget {
  final Pokemon? pokemon;

  const PokemonDetailPage({Key? key, this.pokemon}) : super(key: key);

  @override
  State<PokemonDetailPage> createState() => PokemonDetailPageState();
}

class PokemonDetailPageState extends State<PokemonDetailPage> {
  void addFavorite() {
    if (widget.pokemon!.favorite) {
      widget.pokemon!.favorite = false;
    } else {
      widget.pokemon!.favorite = true;
    }
    setState(() {});
  }

  bodyWidget(BuildContext context) => Stack(
        children: <Widget>[
          Positioned(
            height: MediaQuery.of(context).size.height / 1.8,
            width: MediaQuery.of(context).size.width - 50,
            left: 25,
            top: 80,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50.0),
              ),
              child: Column(
                children: <Widget>[
                  const SizedBox(
                    height: 60,
                  ),
                  Text(
                    widget.pokemon!.name,
                    style: const TextStyle(
                        fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  //Text("id: ${pokemon!.id}"),
                  Text(
                    "N°" + widget.pokemon!.num,
                    style: const TextStyle(
                        fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    widget.pokemon!.height,
                    style: const TextStyle(
                        fontSize: 14, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    widget.pokemon!.weight,
                    style: const TextStyle(
                        fontSize: 14, fontWeight: FontWeight.bold),
                  ),
                  //Text("Type: ${pokemon!.type}"),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "Type",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: widget.pokemon!.type
                        .map((type) => FilterChip(
                            backgroundColor: Pokemon.getColor(type),
                            label: Image.asset(
                              'assets/images/${type.toLowerCase()}.png',
                              height: 25,
                              width: 25,
                            ),
                            onSelected: (type) {}))
                        .toList(),
                  ),
                  // Text("Prev: ${pokemon!.weaknesses}"),
                  const SizedBox(
                    height: 5,
                  ),
                  const Text(
                    "Weaknesses",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: widget.pokemon!.weaknesses!
                        .map((weaknesse) => FilterChip(
                            backgroundColor: Pokemon.getColor(weaknesse),
                            label: Image.asset(
                              'assets/images/${weaknesse.toLowerCase()}.png',
                              height: 25,
                              width: 25,
                            ),
                            onSelected: (weaknesse) {}))
                        .toList(),
                  ),
                  // Text("Prev: ${pokemon!.prevEvolution}"),
                  const SizedBox(
                    height: 5,
                  ),
                  const Text(
                    "Prev",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  widget.pokemon!.prevEvolution!.isEmpty
                      ? const Text(
                          "-",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: widget.pokemon!.prevEvolution!
                              .map((prev) => FilterChip(
                                    backgroundColor: Colors.black,
                                    label: Text(
                                      prev.name as String,
                                      style:
                                          const TextStyle(color: Colors.white),
                                    ),
                                    onSelected: (b) {},
                                  ))
                              .toList(),
                        ),
                  // Text("Next: ${pokemon!.nextEvolution}"),
                  const SizedBox(
                    height: 5,
                  ),
                  const Text(
                    "Next",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  widget.pokemon!.nextEvolution!.isEmpty
                      ? const Text(
                          "-",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: widget.pokemon!.nextEvolution!
                              .map((prev) => FilterChip(
                                    backgroundColor: Colors.black,
                                    label: Text(
                                      "${prev.name}",
                                      style:
                                          const TextStyle(color: Colors.white),
                                    ),
                                    onSelected: (b) {},
                                  ))
                              .toList(),
                        )
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: CachedNetworkImage(
              imageUrl: widget.pokemon!.img,
              imageBuilder: (context, imageProvider) => Container(
                height: 140,
                width: 140,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              placeholder: (context, url) => const CircularProgressIndicator(),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          ),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        backgroundColor: widget.pokemon!.favorite ? Colors.red : Colors.black,
        child: const Icon(Icons.favorite, color: Colors.white),
        onPressed: () => addFavorite(),
      ),
      backgroundColor: widget.pokemon!.backgroundColor,
      appBar: AppBar(
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  widget.pokemon!.backgroundColor,
                  widget.pokemon!.backgroundColor2,
                ],
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                tileMode: TileMode.clamp),
          ),
        ),
        backgroundColor: Colors.black,
        title: Text(widget.pokemon!.name),
      ),
      body: bodyWidget(context),
    );
  }
}
