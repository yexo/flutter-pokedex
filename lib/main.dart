import 'package:flutter/material.dart';
import 'package:flutter_pokedex/widget/pokemonlistpage.dart';

void main() {
  runApp(const App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Accueil',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const PokemonListPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
